#include <iostream>

using namespace std;

// 0100 1010 1010 0010 . 1101 0101 0101 0110
// 0000 0000 0000 0000 . 1111 1111 1111 1111
// 1111 1111 1111 1111 . 0000 0000 0000 0000 
const int scale=16; // 1/2^16
const int FractionMask=0xFFFFFFFF >> (32-scale);
const int WholeMask= -1^FractionMask;

#define DoubleToFixed(x) ((x) * (double)(1<< scale))
#define FixedToDouble(x) ((double)(x)/ (double)(1<<scale))
#define IntToFixed(x)    ((x) << scale)
#define FixedToInt(x)    ((x) >> scale)
#define FractionPart(x)  ((x) & FractionMask)
#define WholePart(x)     ((x) & WholeMask)

// This is really slow
//#define MUL(x, y)        (((long long)(x)*(long long)(y))>>scale)
// This is faster 
#define MUL(x, y)        ((((x)>>8)*((y)>>8))>>0)

// It is too slow
//#define DIV(x, y)        (((long long)(x)<<16)/(y))
// It is better
#define DIV(x, y)        (((x)<<8)/(y)<<8)


int main() {
  cout.precision(20);
  int f = DoubleToFixed(5.7);
  int g = IntToFixed(6);

  // Verifying convertions
  cout << "It's " << FixedToDouble(f) << endl;
  cout << "It's " << FixedToDouble(g) << endl;
  cout << "It's " << (double)(5.7) << endl;

  // Sum two numbers
  int f1 = DoubleToFixed(5.6);
  int f2 = DoubleToFixed(7.9);

  f1 +=f2;
  cout.precision(5);
  cout << "Addition: " << FixedToDouble(f1) << endl;
  
  // Subtraction of two numbers
  f1 = DoubleToFixed(5.6);
  f2 = DoubleToFixed(7.9);  

  f1-=f2;
  cout << "Subtraction: " << FixedToDouble(f1) << endl;

  // The smallest value
  int epsilon=1;
  cout << "It's " << FixedToDouble(epsilon) << endl;

  // Extract the Fraction mask
  f1 = DoubleToFixed(23.6785);
  cout << "Fractional Part: " << FixedToDouble(FractionPart(f1)) << endl;
  cout << "Integer Part: " << FixedToDouble(WholePart(f1)) << endl;
  
  // Multiplication
  int h = DoubleToFixed(7.0);
  h >>= 1;
  cout << "It's " << FixedToDouble(h) << endl;

  int a =  DoubleToFixed(7.0);
  int b =  DoubleToFixed(3.0);

  a = MUL(a, b);
  cout << "Multiplication: " << FixedToDouble(a) << endl;

  a = DIV(a, b);
  cout << "Division:  " << FixedToDouble(a) << endl;

  return 0;
}
